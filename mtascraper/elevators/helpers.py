from datetime import datetime
import pytz

from elevators.models import Elevator, Line, Station, Outage

eastern = pytz.timezone('US/Eastern')

def parse_datestring(datestring):
    d = datetime.strptime(datestring, '%m/%d/%Y %I:%M %p')
    return eastern.localize(d)

def find_or_create_elevator(id, location, station):
    try:
        match = Elevator.objects.get(mta_id=id)
        return match
    except:
        e = Elevator(mta_id=id, location=location, station=station)
        e.save()
        return e

def find_or_create_line(name):
    try:
        match = Line.objects.get(name=name)
        return match
    except:
        l = Line(name=name)
        l.save()
        return l

def find_or_create_station(name, lines):
    try:
        match = Station.objects.filter(name=name, lines__name__in=lines).distinct()[0]
        return match
    except:
        s = Station(name=name)
        s.save()
        ls = [find_or_create_line(l) for l in lines]
        s.lines.set(ls)
        return s

def find_or_create_outage(reason, out_of_service, estimated_return, elevator):
    try:
        match = Outage.objects.get(elevator=elevator, out_of_service=out_of_service)
        return match
    except:
        o = Outage(reason=reason, out_of_service=out_of_service, estimated_return=estimated_return)
        o.elevator = elevator
        o.save()
        return o
