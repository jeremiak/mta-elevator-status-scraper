import datetime

from bs4 import BeautifulSoup
from django.core.management.base import BaseCommand, CommandError
import requests

from elevators.helpers import eastern, parse_datestring, find_or_create_elevator, find_or_create_line, find_or_create_station, find_or_create_outage
from elevators.models import Outage

MTA_URL = 'http://advisory.mtanyct.info/EEoutage/EEOutageReport.aspx?StationID=All'

def get_page_html():
    r = requests.get(MTA_URL)
    if r.status_code is 200:
        return r.text

def get_elevator_table():
    html = get_page_html()
    soup = BeautifulSoup(html, 'html.parser')
    elTable = soup.find(id='ctl00_ContentPlaceHolder1_gvElevator')

    return elTable

def parse_elevator_table_row(row):
    tds = row.find_all('td')

    elevator_num = tds[2].find('h4').text.strip()
    return {
        'ada': tds[0].find(title='ADA-compliant'),
        'station': tds[0].find('h4').text.strip(),
        'lines': [i['alt'].split(' line subway')[0] for i in tds[1].find_all('img')],
        'elevator_num': elevator_num if '*' not in elevator_num else elevator_num.split('\r')[0],
        'location': tds[3].find('h4').text.strip(),
        'out_of_service': tds[4].find('h4').text.strip(),
        'reason': tds[5].find('h4').text.strip(),
        'estimated_return': tds[6].find('h4').text.strip(),
    }

def scrape_elevator_table():
    now = datetime.datetime.now()
    table = get_elevator_table()
    trs = table.find_all('tr')
    outages = [parse_elevator_table_row(row) for row in trs[1:]]
    return {
        'scraped_at': now,
        'outages': outages,
    }


class Command(BaseCommand):
    help = 'Scrape elevator statuses'

    def handle(self, *args, **options):
        self.stdout.write('scraping and saving')
        data = scrape_elevator_table()
        outages = data['outages']
        now = eastern.localize(data['scraped_at'])
        active = []
        self.stdout.write('processing %s active outages' % len(outages))
        for i, o in enumerate(outages):
            # self.stdout.write('%s' % o)
            out_of_service = parse_datestring(o['out_of_service'])
            estimated_return = parse_datestring(o['estimated_return'])
            station = find_or_create_station(o['station'], o['lines'])
            # self.stdout.write('%s' % station)
            elevator = find_or_create_elevator(o['elevator_num'], o['location'], station)
            # self.stdout.write('%s' % elevator)
            outage = find_or_create_outage(o['reason'], out_of_service, estimated_return, elevator)
            # self.stdout.write('%s' % outage)
            active.append(outage.id)
            self.stdout.write('done with %s of %s' % (i + 1, len(outages)))

        first_missed = Outage.objects.exclude(id__in=active).exclude(first_missed__isnull=False)
        self.stdout.write('noting the first miss of %s outages' % len(first_missed))
        for i, o in enumerate(first_missed):
            o.first_missed = now
            o.save()
            self.stdout.write('done with %s of %s' % (i + 1, len(first_missed)))
        self.stdout.write('done scraping')
