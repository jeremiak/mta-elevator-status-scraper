from csv import DictWriter as csv
import datetime

import boto3 as boto
from django.core.management.base import BaseCommand, CommandError

from elevators.helpers import eastern, parse_datestring, find_or_create_elevator, find_or_create_line, find_or_create_station, find_or_create_outage
from elevators.models import Outage

client = boto.client('s3') 

def create_full_outage_row_object(outage):
    row = {}
    elevator = outage.elevator
    station = elevator.station
    lines = [l.name for l in station.lines.all()]

    row['elevator_mta_id'] = elevator.mta_id
    row['elevator_location'] = elevator.location
    row['reason'] = outage.reason
    row['out_of_service'] = "%s" % outage.out_of_service
    row['estimated_return'] = "%s" % outage.estimated_return
    row['first_missed'] = "%s" % outage.first_missed
    row['station_name'] = station.name
    row['station_lines'] = ",".join(lines)

    return row

def write_dict_array_to_csv(filename, rowArray):
    with open(filename, 'w', newline='') as csvfile:
        fieldnames = rowArray[0].keys()
        writer = csv(csvfile, fieldnames=fieldnames)

        writer.writeheader()
        for row in rowArray:
            writer.writerow(row)

class Command(BaseCommand):
    help = 'Publish data to S3'

    def handle(self, *args, **options):
        self.stdout.write('publishing data as csv to S3')
        # now = eastern.localize(data['scraped_at'])
        all_outages = Outage.objects.all()
        self.stdout.write('processing %s outages' % len(all_outages))
        
        rows = [create_full_outage_row_object(o) for o in all_outages]
        write_dict_array_to_csv('mta_elevator_outages.csv', rows)

        self.stdout.write('csv created with %s data rows' % len(rows))

        client.upload_file('mta_elevator_outages.csv', 'nyc-mta-elevator-outages', 'mta_elevator_outages.csv')
        self.stdout.write('done publishing')
