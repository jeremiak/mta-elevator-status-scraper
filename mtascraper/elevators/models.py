from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

class Line(models.Model):
    name = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.name

class Station(models.Model):
    name = models.CharField(max_length=200)
    lines = models.ManyToManyField(Line)
    year_built = models.IntegerField(
        blank=True,
        null=True,
        validators = [
            MaxValueValidator(2018),
            MinValueValidator(1870)
        ]
    )

    def __str__(self):
        return '%s - (ID: %s)' % (self.name, self.id)

class Elevator(models.Model):
    mta_id = models.CharField(max_length=10, unique=True)
    station = models.ForeignKey(Station, on_delete=models.CASCADE)
    location = models.CharField(max_length=200)

    def __str__(self):
        return self.mta_id

class Outage(models.Model):
    elevator = models.ForeignKey(Elevator, on_delete=models.CASCADE)
    reason = models.TextField()
    out_of_service = models.DateTimeField()
    estimated_return = models.DateTimeField()
    first_missed = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return "%s - %s" % (self.elevator, self.reason)
