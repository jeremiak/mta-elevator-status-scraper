from django.apps import AppConfig


class ElevatorsConfig(AppConfig):
    name = 'elevators'
