from django.contrib import admin

from .admin_actions import export_as_csv_action
from .models import Elevator, Line, Outage, Station

class ElevatorAdmin(admin.ModelAdmin):
    list_display = ('mta_id', 'station', 'location')
    actions = [export_as_csv_action(fields=list_display)]

class OutageAdmin(admin.ModelAdmin):
    list_display = ('elevator', 'reason', 'out_of_service', 'estimated_return', 'first_missed')
    actions = [export_as_csv_action(fields=list_display)]

class StationAdmin(admin.ModelAdmin):
    list_display = ('name', 'id', 'year_built')
    actions = [export_as_csv_action(fields=list_display)]

admin.site.register(Elevator, ElevatorAdmin)
admin.site.register(Line)
admin.site.register(Outage, OutageAdmin)
admin.site.register(Station, StationAdmin)
