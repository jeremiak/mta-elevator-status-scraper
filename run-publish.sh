#!/bin/bash
cd $(dirname $0)
exec &>> capture-log.txt
echo "Running publish job at $(date)"
/usr/local/bin/docker-compose run --rm web python mtascraper/manage.py publish
