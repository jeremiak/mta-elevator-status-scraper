FROM python:3

WORKDIR /app
ADD requirements.txt /app/
RUN apt-get update && \
  pip install -r requirements.txt
